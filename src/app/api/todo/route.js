import clientPromise from "@/lib/mongodb";
import { ObjectId } from "mongodb";
import { NextResponse } from "next/server";


export async function GET(request) {
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");

  try {
    const todos = await collection.find({}).toArray();
    return NextResponse.json(todos, { 
      status: 200, 
      headers:{ 
        // "Access-Control-Allow-Origin": "https://todo-fe-kanomnutt.azurewebsites.net",
        "Content-Type": "application/json",
       } });
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}

export async function POST(request) {
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");
  const { text, description, duedate } = await request.json();

  try {
    const currentDate = new Date();
    const day = String(currentDate.getDate()).padStart(2, '0');
    const month = String(currentDate.getMonth() + 1).padStart(2, '0');
    const year = currentDate.getFullYear();
    const formattedDate = `${year}-${month}-${day}`;

    const todo = { 
      text: text, 
      description: description, 
      completed: false, 
      createdDate: formattedDate, 
      duedate: duedate };

    await collection.insertOne(todo);
    return NextResponse.json(todo, { 
      status: 201,
      headers: {
        // 'Access-Control-Allow-Origin': origin,
        "Content-Type": "application/json",
      }
    });
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}

export async function PUT(request) {
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");
  const { id, text, description, duedate } = await request.json();

  try {
    await collection.updateOne(
      { _id: new ObjectId(id) },
      { $set: { text, description, duedate } }
    );
    return NextResponse.json(
      { message: "Todo updated successfully" },
      { 
        status: 200,
        headers: {
          // 'Access-Control-Allow-Origin': 'https://todo-fe-kanomnutt.azurewebsites.net',
          "Content-Type": "application/json",
        }
      }
    );
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}

export async function DELETE(request) {
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");
  const { id } = await request.json();

  try {
    await collection.deleteOne({ _id: new ObjectId(id) });
    return NextResponse.json(
      { message: "Todo deleted successfully" },
      { 
        status: 200,
        headers: {
          // 'Access-Control-Allow-Origin': 'https://todo-fe-kanomnutt.azurewebsites.net',
          "Content-Type": "application/json",
        }
      }
    );
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}

