import clientPromise from "@/lib/mongodb";
import { ObjectId } from "mongodb";
import { NextResponse } from "next/server";


export async function POST(request) {
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");
  const { id, completed } = await request.json();

  try {
    await collection.updateOne(
      { _id: new ObjectId(id) },
      { $set: { completed } }
    );
    return NextResponse.json(
      { 
        status: 200,
        headers: {
          // 'Access-Control-Allow-Origin': origin,
          "Content-Type": "application/json",
        }
      }
    );
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}

export async function GET(request) {
  console.log(request.nextUrl.pathname.split('/').pop())
  const client = await clientPromise;
  const collection = client.db("todoapp").collection("todos");
  // const id = request.url.split('/').pop();
  const id  = request.nextUrl.pathname.split('/').pop();
  try {
    const todos = await collection.findOne({
      _id: new ObjectId(id)
    });
    return NextResponse.json(todos, { 
      status: 200, 
      headers:{ 
        // "Access-Control-Allow-Origin": "https://todo-fe-kanomnutt.azurewebsites.net",
        "Content-Type": "application/json",
       } },
       );
  } catch (error) {
    return NextResponse.json(error, { status: 500 });
  }
}
