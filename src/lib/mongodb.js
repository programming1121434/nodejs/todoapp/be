import { MongoClient } from "mongodb";

// const {MongoClient} = require('mongodb');

const uri = process.env.MONGODB_URI;

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}

if(!process.env.MONGODB_URI){
    throw new Error('Please add MongoDB connection string to .env file')
}

const client = new MongoClient(uri, options);
const clientPromise = client.connect();

clientPromise.then(() => console.log('Connected to MongoDB'));

export default clientPromise